package com.example.appadore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class PreviewActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private List<Fragment> listFragments = new ArrayList<>();
//    private List<String> listImageURLs = new ArrayList<>();
    String listImageURLs;

    private int position = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        getArguments();
        createFragments();

        viewPager = findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), listFragments);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(position);
    }

    private void getArguments(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            listImageURLs = bundle.getString("imageURLs");
            position = bundle.getInt("position");
            Log.e("paths", String.valueOf(listImageURLs));
        }
    }

    private void createFragments(){
//        for(int i=0;i<listImageURLs.size();i++){
            Bundle bundle = new Bundle();
            bundle.putString("imageURL", listImageURLs);
            ImageFragment imageFragment = new ImageFragment();
            imageFragment.setArguments(bundle);
            listFragments.add(imageFragment);
//        }
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> listFragment;

        public ViewPagerAdapter(FragmentManager fragmentManager, List<Fragment> listFragment) {
            super(fragmentManager);
            this.listFragment = listFragment;
        }

        @Override
        public Fragment getItem(int position) {
            return listFragment.get(position);
        }

        @Override
        public int getCount() {
            return listFragment.size();
        }
    }
}