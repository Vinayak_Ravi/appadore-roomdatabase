package com.example.appadore;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    TextView text_details;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        text_details = findViewById(R.id.text_details);
        back = findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String details = "<b>"+"Step:1"+"</b>"+" Created an application which has two tabs [Gallery | Camera]"+ "<br/>"+"<b>"+"Step:2"+"</b>"+" Added a button to add photos"+"<br/>"+"<b>"+"Step:3"+"</b>"+" If the button is clicked it will ask whether to upload from gallery or camera"+ "<br/>"
        +"<b>"+"Step:4"+"</b>" +" If the Gallery is selected, open the gallery view and the user can select one or multiple images and upload it"+"<br/>"+"<b>"+"Step:5"+"</b>"+" If the camera is selected, open the camera and the user can take photos to upload it" +"<br/>"+
                "<b>"+"Step:6"+"</b>"+  " Before Uploading user can preview"+"<br/>"+"<b>"+"Step:7"+"</b>"+" When upload is pressed save the photo in the RoomDatabase and show it in the respective tab";

        text_details.setText(Html.fromHtml(details));

    }
}