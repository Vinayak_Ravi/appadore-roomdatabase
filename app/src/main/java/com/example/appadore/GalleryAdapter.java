package com.example.appadore;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder> {

    private List<ImageDatas> taskList;


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img_view;


        public MyViewHolder(View view) {
            super(view);

            img_view = view.findViewById(R.id.image_view);

        }

    }

    Context context;

    public GalleryAdapter(List<ImageDatas> imageView_models, Context mContext) {
        this.taskList = imageView_models;
        this.context = mContext;
//        notifyDataSetChanged();

    }


    @Override
    public GalleryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.imageitem_view, parent, false);

        return new GalleryAdapter.MyViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        ImageDatas t = taskList.get(position);

        Glide.with(context).load(t.getImage_url()).into(holder.img_view);



        holder.img_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("img",t.getImage_url());
                if (t.getImage_url()!="" &&t.getImage_url()!=null) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("position", position);
                    bundle.putString("imageURLs", t.getImage_url());
//                Toast.makeText(context, t.getImage_uri(), Toast.LENGTH_LONG).show();
                    Intent i = new Intent(context, PreviewActivity.class);
                    i.putExtras(bundle);
                    context.startActivity(i);
                }else {
                    Toast.makeText(context, "Image has been deleted from your Phone", Toast.LENGTH_LONG).show();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

}
