package com.example.appadore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class Image_View extends AppCompatActivity {
    RoomDB roomDB;
    RecyclerView recyclerView;
    private ImageViewAdapter imageViewAdapter;
    private List<ImageView_Model> imageView_models = new ArrayList<>();
    List<ImageDatas> datasList = new ArrayList<>();

    String array_path;
    TextView done;
    JSONArray img_arr;
    ImageView back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image__view);
        roomDB = RoomDB.getInstance(Image_View.this);
        datasList = roomDB.imageDAO().getAll();
        array_path = getIntent().getStringExtra("url_Path");

        recyclerView = findViewById(R.id.recyclerView);
        back = findViewById(R.id.back);
        done = findViewById(R.id.done);
        imageViewAdapter = new ImageViewAdapter(imageView_models, this);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(Image_View.this, 3);
        recyclerView.setLayoutManager(mLayoutManager);

        try {
            img_arr = new JSONArray(array_path);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        imageView_Items();

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               roomDB();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private void roomDB(){
        final ProgressDialog dialog = new ProgressDialog(Image_View.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        int size = img_arr.length();
        for (int i = 0; i < img_arr.length(); i++ ){
            try {
                ImageDatas datas = new ImageDatas();
                String path = String.valueOf(img_arr.get(i)) ;
                datas.setImage_url(path);
                roomDB.imageDAO().insert(datas);
                datasList.addAll(roomDB.imageDAO().getAll());
//                Toast.makeText(this,path,Toast.LENGTH_SHORT).show();
                Log.e("check",String.valueOf(datasList.addAll(roomDB.imageDAO().getAll())));

            } catch (JSONException e) {
                Log.e("err----", String.valueOf(e));
                e.printStackTrace();
            }

        }

        dialog.dismiss();

        Toast.makeText(Image_View.this, "success", Toast.LENGTH_SHORT).show();
        finish();
    }

    private void imageView_Items() {

        img_arr.length();

        for (int i = 0; i < img_arr.length(); i++) {
            String urlpath;
            try {
                urlpath = String.valueOf(img_arr.get(i));

                ImageView_Model view_model = new ImageView_Model();
                view_model.setImage_uri(urlpath);
                imageView_models.add(view_model);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        recyclerView.setAdapter(imageViewAdapter);
        imageViewAdapter.notifyItemInserted(imageView_models.size() - 1);

        imageViewAdapter.notifyDataSetChanged();
    }
}