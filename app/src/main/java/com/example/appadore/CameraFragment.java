package com.example.appadore;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.example.appadore.MainActivity.camera;


public class CameraFragment extends Fragment {

    Button add_Photos;
    List<String> imageVideoPathList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_camera, container, false);

        add_Photos = v.findViewById(R.id.add_Photos);

        add_Photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.performClick();
            }
        });

        return v;
    }





    private void selectImage(){
        final CharSequence[] items ={"Camera","Gallery","Cancel"};

        AlertDialog.Builder builder =new AlertDialog.Builder(getContext());
        builder.setTitle("Add Images");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if(items[i].equals("Camera")){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent,1);
                }else if(items[i].equals("Gallery")){
                    ImagePicker.create(getActivity())
                            .includeVideo(false)
                            .limit(2)
                            .folderMode(true)
                            .toolbarFolderTitle("Select Folder") // folder selection title
                            .multi()
//                            .theme(R.style.PickerTheme) // must inherit ef_BaseTheme. please refer to sample
                            .enableLog(false) // disabling log
                            .start();



                }
                else {
                    dialog.dismiss();
                }
            }


        });
        builder.show();
    }



    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
        Log.e("data of intent", String.valueOf(data));

        Toast.makeText(getContext(),"hello",Toast.LENGTH_SHORT).show();

        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            List<Image> images = ImagePicker.getImages(data);
            // or get a single image only
            imageVideoPathList = new ArrayList<>();

            Log.e("size", String.valueOf(images.size()));
            for (int i = 0; i < images.size(); i++) {
                String path = images.get(i).getPath();
                Log.e("image_id su", path);
                if (path.endsWith("bmp") || path.endsWith("jpg") || path.endsWith("jpeg") || path.endsWith("png") ||
                        path.endsWith("webp") || path.endsWith("heic") || path.endsWith("heif") || path.endsWith("mp4")) {
                    imageVideoPathList.add(path);


                }

                Log.e("----path----", String.valueOf(imageVideoPathList));


            }
        }


    }




}