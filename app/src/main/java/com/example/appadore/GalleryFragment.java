package com.example.appadore;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;


public class GalleryFragment extends Fragment {

    RecyclerView recyclerView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(mLayoutManager);


        
        getTasks();

        return view;
    }


    private void getTasks() {
        class GetTasks extends AsyncTask<Void, Void, List<ImageDatas>> {

            @Override
            protected List<ImageDatas> doInBackground(Void... voids) {
                List<ImageDatas> taskList = RoomDB
                        .getInstance(getActivity().getApplicationContext())
                        .imageDAO()
                        .getAll();
                return taskList;
            }

            @Override
            protected void onPostExecute(List<ImageDatas> tasks) {
                super.onPostExecute(tasks);
                GalleryAdapter adapter = new GalleryAdapter(tasks, getActivity());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }


    @Override
    public void onResume() {
        super.onResume();
        getTasks();
    }
}
