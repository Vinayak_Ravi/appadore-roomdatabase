package com.example.appadore;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ImageDAO {

    @Insert(onConflict = REPLACE)
    void insert(ImageDatas imageDatas);

    @Delete
    void delete(ImageDatas imageDatas);

    @Delete
    void reset(List<ImageDatas> imageDatas);


    @Query("UPDATE images SET text = :sText WHERE ID = :sID")
    void update(int sID, String sText);

    @Query("SELECT * FROM images")
    List<ImageDatas> getAll();
}
