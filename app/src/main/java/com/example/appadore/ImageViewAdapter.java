package com.example.appadore;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class ImageViewAdapter extends RecyclerView.Adapter<ImageViewAdapter.MyViewHolder> {
    private List<ImageView_Model> imageView_models;
    int size= 0 ;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img_view;

        public MyViewHolder(View view) {
            super(view);


            img_view = view.findViewById(R.id.image_view);

        }

    }

    Context context;

    public ImageViewAdapter(List<ImageView_Model> imageView_models, Context mContext) {
        this.imageView_models = imageView_models;
        this.context = mContext;

    }

    @Override
    public ImageViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.imageitem_view, parent, false);

        return new ImageViewAdapter.MyViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

//        size = imageView_models.size()+1;

//        if (position == 0) {
//
//            Glide.with(context).load("").placeholder(R.drawable.ef_folder_placeholder)
//                    .into(holder.img_view);
//        } else {


            final ImageView_Model view_model = imageView_models.get(position);
        Log.e("size", String.valueOf(imageView_models.size()));

            Glide.with(context).load(view_model.getImage_uri()).into(holder.img_view);

            holder.img_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("position", position);
                    bundle.putString("imageURLs", view_model.getImage_uri());
                    Toast.makeText(context, view_model.getImage_uri(), Toast.LENGTH_LONG).show();
                    Intent i = new Intent(context, PreviewActivity.class);
                    i.putExtras(bundle);
                    context.startActivity(i);
                }
            });
//        }

    }

    @Override
    public int getItemCount() {
        return imageView_models.size();
    }

}
